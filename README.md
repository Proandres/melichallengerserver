# SC API

This is a RestAPI

## Folder Structure
```

/src
  /controllers -- business logic for handling API endpoints
  /helpers -- modules for common functions that sit outside controllers/models
  /routes -- defines API endpoints and passes requests to corresponding controllers
    /api -- api routes that need a valid authentication
    /middlewares -- main handler of routes validations
    index.js -- the main Express app
  /services -- all business logic should be here
  /util -- functions that are often used should be here 
  `app.js` -- Main app creation, everything converge here, `middlewares` u need and pre config for the express openapi
package.json -- defines scripts for utilities like migrations and seeds
.env -- Needed for database,port and email
```

## .env Structure


``
#### PAGE_SIZE -- max size for api response
```
example
PAGE_SIZE=50
```

#### PORT
```
PORT=2322
```
#### NODE_ENV
```
NODE_ENV=production # if u go on production
NODE_ENV=development # if u go on for develop purpose
``