const { ItemsController } = include('controllers');

module.exports = router => {
    router.get('/:id', ItemsController.fetchItems);
    router.get('/', ItemsController.fetchItems);
    return router;
};
