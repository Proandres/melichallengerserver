const { Router } = require('express');
const get = require('lodash/get');
const {errorHandler} = require('./middleWares');

const { StatusController } = include('controllers');

const Logger = include('helpers/logger');
const Error = include('helpers/error');

const localRoute = route => {
    route.get('/ping', StatusController.ping);
    route.get('/swagger', (_, res) => res.send(get(include('openapi'), 'components')));
    route.get('/', (_, res) => Error.send404(res));
    route.get('*', (_, res) => Error.sendError(res));
    return route;
};

class Routes {
    static configure(app) {
        app.use('/api', require('./api')(Router()));
        Logger.info('Loading public-api...');
        app.use('/', localRoute(Router()));
        app.use(errorHandler);
    }
}

module.exports = Routes;
