/* eslint-disable no-unused-vars */
const fs = require('fs');
const express = require('express')();
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const helmet = require('helmet');
const swaggerUi = require('swagger-ui-express');
const {middleware} = require('express-openapi-validator');

const spdy = require('spdy');

const logger = require('./helpers/logger');
const loggerHttp = require('./helpers/loggerHttp');

const Router = require('./routes');
const packageJson = require('../package.json');

const {
    BODY_LIMIT,
    NODE_ENV,
    PORT
} = process.env;

class App {
    constructor() {
        /**
         * @var {express} app
         */
        this.app = express;
    }

    test() {
        this._routes();
        return express;
    }

    _onListening() {
        if(NODE_ENV !== 'test') {
            logger.info(`Started ${packageJson.name} at port ${PORT} in ${NODE_ENV} environment`);
        }
    }

    _onError(err) {
        logger.error(`App Crashed, Error: ${err.errorMessage}`);
        process.exit;
    }

    init() {
        if(NODE_ENV !== 'test') {
            this._configure();
            if (NODE_ENV === 'production') {
                if (process.env.CERT_KEY && process.env.CERT_PATH) {
                    const options = {
                        key: fs.readFileSync(process.env.CERT_KEY),
                        cert: fs.readFileSync(process.env.CERT_PATH),
                        spdy: {protocols: ['h2', 'http/1.1', 'http/2']}
                    };
                    spdy.createServer(options, express).listen(PORT, err => {
                        if (err) {
                            return this._onError(err);
                        }
                        this._onListening();
                    });
                } else {
                    express.listen(PORT, this._onListening);
                    express.on('error', this._onError);
                }
            } else {
                express.listen(PORT, this._onListening);
                express.on('error', this._onError);
            }
            return express;
        }
    }

    _configure() {
        this._middleWares();
        return this._routes();
    }

    _middleWares() {
        express.use(cookieParser());
        if (NODE_ENV !== 'test') {
            express.use(function (req, res, next) {
                function afterResponse() {
                    res.removeListener('finish', afterResponse);
                    res.removeListener('close', afterResponse);
                    loggerHttp.loggerInstance(res);
                }
                res.on('finish', afterResponse);
                res.on('close', afterResponse);
                next();
            });
        }
        if (NODE_ENV === 'development') {
            express.use(cors({
                credentials: true,
                origin: /^http:\/\/localhost/
            }));
        } else if(NODE_ENV !== 'test') {
            express.disable('x-powered-by');
            express.use(helmet());
            express.use(helmet.noSniff());
            express.use(helmet.referrerPolicy({ policy: 'same-origin' }));
            express.use(helmet.contentSecurityPolicy({
                directives: {
                    defaultSrc: ['\'self\''],
                    styleSrc: ['\'self\'', 'maxcdn.bootstrapcdn.com']
                }
            }));
            const sixtyDaysInSeconds = 15768000;
            express.use(helmet.hsts({maxAge: sixtyDaysInSeconds}));
            express.use(cors());
        }
        return;
    }

    _routes() {
        const apiSpec = include('openapi');
        const options = {swaggerOptions: {validatorUrl: null}};

        express.use('/docs', swaggerUi.serve, swaggerUi.setup(apiSpec, options));

        express.use(middleware({
            apiSpec,
            validateRequests: true,
            validateResponses: false
        }));
        Router.configure(express);
        return;
    }
}

module.exports = App;
