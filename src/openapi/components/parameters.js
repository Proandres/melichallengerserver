module.exports = {
    Size: {
        in: 'query',
        name: 'size',
        schema: {
            type: 'number',
            default: 0,
            minimum: 0
        }
    },
    Skip: {
        in: 'query',
        name: 'skip',
        schema: {
            type: 'number',
            default: 0,
            minimum: 0
        }
    }
};
