module.exports = {
    type: 'object',
    properties: {
        _id: {type: 'number'},
        name: {type: 'string'},
        description: {type: 'string'},
        background_color: {type: 'string'},
        text_color: {type: 'string'},
        status: {type: 'string'},
        deleted: {type: 'boolean'},
        createdAt: {
            type: 'string',
            format: 'date'
        },
        updatedAt: {
            type: 'string',
            format: 'date'
        },
        deletedAt: {
            type: 'string',
            format: 'date'
        }
    }
};
