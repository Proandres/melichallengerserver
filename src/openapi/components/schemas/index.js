const Items = require('./items');

module.exports = {
    Items,
    ArrayString: {
        type: 'array',
        uniqueItems: true,
        items: {type: 'string'}
    },
    ArrayNumber: {
        type: 'array',
        uniqueItems: true,
        items: {type: 'integer'}
    },
    ids: {
        type: 'array',
        uniqueItems: true,
        items: {
            type: 'string',
            format: 'uuid'
        }
    },
    Error: {
        type: 'object',
        required: [
            'code',
            'message'
        ],
        properties: {
            code: {
                type: 'integer',
                format: 'int32'
            },
            message: {type: 'string'}
        }
    }
};
