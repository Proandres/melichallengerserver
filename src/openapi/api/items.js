const PATH = '/api/items';

module.exports = {
    [PATH]: {
        get: {
            // security: [{bearerAuth: []}],
            summary: 'Get list of items',
            description: 'Get list of items that can be filtered in query params',
            operationId: 'fetch',
            parameters: [
                {
                    in: 'query',
                    name: 'q',
                    schema: {type: 'string'}
                },
                {$ref: '#/components/parameters/Size'},
                {$ref: '#/components/parameters/Skip'}
            ],
            responses: {
                200: {
                    description: 'list of available item',
                    content: {
                        'application/json': {
                            schema: {
                                type: 'array',
                                items: {$ref: '#/components/schemas/Items'}
                            }
                        }
                    }
                },
                400: {description: 'Invalid Request'}
            }
        }
    },
    [`${PATH}/{id}`]: {
        get: {
            // security: [{bearerAuth: []}],
            summary: 'Get a item',
            description: 'Get a item based on id of the same',
            operationId: 'fetch',
            parameters: [
                {
                    in: 'path',
                    name: 'id',
                    schema: {type: 'string'},
                    required: true
                },
                {
                    in: 'query',
                    name: 'q',
                    schema: {type: 'string'}
                }
            ],
            responses: {
                200: {
                    description: 'One item',
                    content: {'application/json': {schema: {$ref: '#/components/schemas/Items'}}}
                },
                400: {description: 'Invalid Request'}
            }
        }
    }
};
