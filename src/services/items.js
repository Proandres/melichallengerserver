const fetch = require('node-fetch');
const {API} = process.env;

class ItemsService {
    static async fetchItems({q}) {
        const fetchInfo = await fetch(`${API}/sites/MLA/search?q=${q}&limit=4`, {method: 'GET'});
        const result = await fetchInfo.json();
        return result;
    }

    static async fetchItem({id}) {
        const fetchItem = await fetch(`${API}/items/${id}`, {method: 'GET'});
        const item = await fetchItem.json();
        return {item };
    }

    static async fetchDescription({id}) {
        const fetchDescription = await fetch(`${API}/items/${id}/description`, {method: 'GET'});
        const description = await fetchDescription.json();
        return {description};
    }

    static async fetchCategory({id}) {
        const fetchCategory = await fetch(`${API}/categories/${id}`, {method: 'GET'});
        const category = await fetchCategory.json();
        return {category};
    }
}

module.exports = ItemsService;
