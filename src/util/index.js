const get = require('lodash/get');
const join = require('lodash/join');
const pick = require('lodash/pick');
const reduce = require('lodash/reduce');
const values = require('lodash/values');
const map = require('lodash/map');
const find = require('lodash/find');
const assign = require('lodash/assignIn');
const orderBy = require('lodash/orderBy');

const reducedList = (array, filterKey, keyData) => reduce(array, (objectsByKeyValue, obj) => {
    const value = join(values(pick(obj, filterKey)), '');
    objectsByKeyValue[value] = (objectsByKeyValue[value] || []).concat({
        name: get(obj, keyData),
        _id: get(obj, keyData)
    });
    return objectsByKeyValue;
}, {});

const setItem = item => {
    return {
        id: get(item, 'id'),
        title: get(item, 'title'),
        price: {
            currency: get(item, 'currency_id'),
            amount: get(item, 'price'),
            decimals: 0
        },
        picture: get(item, 'thumbnail'),
        condition: get(item, 'condition'),
        free_shipping: get(item, 'shipping.free_shipping'),
        location: get(item, 'seller_address.state.name'),
        category: get(item, 'category_id')
    };
};

const getAuthor = () => {
    return {
        name: 'Andres',
        lastname: 'Prommel'
    };
};

const generateResponseItems = apiResponse => {
    const items = map(apiResponse.results, item => setItem(item));
    const currentCategories = find(apiResponse.available_filters, {id: 'category'});
    const categories = map(orderBy(currentCategories.values, ['results'], ['desc']), item => item.name);
    const response = {
        author: getAuthor(),
        categories: categories,
        items
    };
    return response;
};

const generateResponseOneItem = item => {
    const newItem = setItem(item);
    assign(newItem, {
        description: get(item, 'description.plain_text'),
        sold_quantity: get(item, 'sold_quantity')
    });
    return newItem;
};

const sendResponse = (data, success) => {
    if(!success) {
        return {
            success: false,
            data: null,
            message: 'No se pudo Completar la Accion'
        };
    }
    return {
        success: true,
        data,
        message: 'La accion se completo con exito'
    };
};

module.exports = {
    reducedList,
    generateResponseItems,
    sendResponse,
    generateResponseOneItem,
    getAuthor,
    setItem
};
