const pkg = require('../../package');

class StatusController {
    static ping(req, res, next) {
        try {
            res.send({
                version: pkg.version,
                status: 'running'
            });
        } catch (err) {
            next(err);
        }
    }
}

module.exports = StatusController;
