const assign = require('lodash/assignIn');
const set = require('lodash/set');
const get = require('lodash/get');
const {ItemsServices} = require('../services');
const {
    generateResponseItems, sendResponse, generateResponseOneItem
} = include('/util');

class ItemsController {
    static async fetchItems(req, res, next) {
        try {
            const {q} = req.query;
            const { id } = req.params;
            if(id) {
                const {item} = await ItemsServices.fetchItem({id});
                const {description} = await ItemsServices.fetchDescription({id});
                assign(item, {description: description});
                const {category} = await ItemsServices.fetchCategory({id: item.category_id});
                set(item, 'category_id', get(category, 'name'));
                const response = generateResponseOneItem(item);
                return res.status(201).send(sendResponse(response, true));
            }
            const documents = await ItemsServices.fetchItems({q});
            const response = generateResponseItems(documents);
            if(response) {
                return res.status(201).send(sendResponse(response, true));
            }
            return res.status(400).send(sendResponse(false));
        } catch (err) {
            next(err);
        }
    }
}

module.exports = ItemsController;
